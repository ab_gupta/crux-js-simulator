package in.sandbox.crux;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import in.zeta.crux.enums.ResourceState;
import in.zeta.crux.enums.ScriptFormat;
import in.zeta.crux.enums.TransformerScriptType;
import in.zeta.crux.factory.transformer.JsTransformerExecutor;
import in.zeta.crux.factory.transformer.TransformerFactory;
import in.zeta.crux.model.TransformerFunction;
import in.zeta.crux.model.TransformerFunction.Argument;
import in.zeta.crux.model.TransformerScript;
import in.zeta.crux.providers.EmbeddedDataFileObjectProvider;
import in.zeta.crux.resourceobjects.Transformer;
import in.zeta.crux.scriptengine.ZetaHardenedScriptEngineFactory;
import in.zeta.crux.services.TransformerService;
import in.zeta.delta.library.services.ResourceObjectService;
import in.zeta.delta.library.util.SpartanUtil;
import in.zeta.gateway.common.gwclients.models.paymentcode.response.PaymentCode;
import in.zeta.gateway.common.gwclients.models.paymentcode.response.Rule;
import in.zeta.iso.commons.model.ISOMessageAttributes;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;

public class CruxTest {

  final long ifi;
  final List<PaymentCode> paymentCodes = new ArrayList<>();
  final TransformerService transformerService;
  final TransformerFactory factory;
  final SpartanUtil spartanUtil;
  final EmbeddedDataFileObjectProvider embeddedDataFileObjectProvider;
  private final Gson gson;


  public CruxTest() throws IOException {
    ifi = 140827L;
    gson = new Gson();
    embeddedDataFileObjectProvider = new EmbeddedDataFileObjectProvider(
        new ResourceObjectService(null, null, gson));
    spartanUtil = new SpartanUtil();
    factory = new TransformerFactory(
        new JsTransformerExecutor(ZetaHardenedScriptEngineFactory::getInstance,
            embeddedDataFileObjectProvider, spartanUtil, gson));
    transformerService = new TransformerService(embeddedDataFileObjectProvider, factory);
    Class<CruxTest> cruxClass = CruxTest.class;
    paymentCodes.addAll(loadPaymentCodesFromResourceFiles(cruxClass));
  }

  private List<PaymentCode> loadPaymentCodesFromResourceFiles(Class<CruxTest> cruxClass) throws IOException {
    final List<PaymentCode> paymentCodes = new ArrayList<>();
    for (Constants.TxnType txnType : Constants.TxnType.values()) {
      try (InputStream resourceAsStream = cruxClass.getResourceAsStream("/paymentcode/" + ifi + "_" + txnType.getName() + ".js")) {
        if (resourceAsStream != null) {
          String data = IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
          paymentCodes.add(new Gson().fromJson(data, PaymentCode.class));
        }
      }
    }
    return paymentCodes;
  }

  public static void main(String[] args) throws IOException {
    final CruxTest cruxTest = new CruxTest();
    for (int i = 0; i < 1000; i++) {
      cruxTest.simulateV2(i, 140827L);
    }
    System.exit(0);
  }

    public void simulateV2(int iteration, long tenantID) {
      Map<String, JsonElement> transformerArguments = getTransformerArguments(getIsoMessageAttributes());
      List<CompletableFuture<String>> resultsFutures = getTransformerMap()
              .entrySet().stream().map(entry -> {
                return getTransformerService().apply(tenantID, entry.getValue(), transformerArguments)
                        .thenApply(JsonElement::getAsString)
                        .thenApply(result -> {
                          if("PASSED".equalsIgnoreCase(result)) {
                            System.out.println(iteration + ") " + " resolved PC : " + entry.getKey());
                          }
                          return result;
                        });
              }).map(CompletionStage::toCompletableFuture).collect(Collectors.toList());

      CompletableFuture.allOf(resultsFutures.toArray(new CompletableFuture[0]))
              .thenAccept(aVoid -> {
                 List<String> results = resultsFutures.stream()
                         .map(CompletableFuture::join)
                         .filter("PASSED"::equalsIgnoreCase)
                         .collect(Collectors.toList());
                 if(results.size() != 1) {
                   System.out.println(iteration + ") ERROR " + " resolved PC : " + results.size());
                 }
              }).toCompletableFuture().join();
    }

  public void simulateV2Sequential(int iteration, long tenantID) {
    Map<String, JsonElement> transformerArguments = getTransformerArguments(getIsoMessageAttributes());
    List<String> results = getTransformerMap()
            .entrySet().stream().map(entry -> {
              return transformerService.apply(tenantID, entry.getValue(), transformerArguments)
                      .thenApply(JsonElement::getAsString)
                      .thenApply(result -> {
                        if("PASSED".equalsIgnoreCase(result)) {
//                          System.out.println(iteration + ") " + " resolved PC : " + entry.getKey());
                        }
                        return result;
                      }).toCompletableFuture().join();
            })
            .filter("PASSED"::equalsIgnoreCase)
            .collect(Collectors.toList());
    if(results.size() != 1) {
      System.out.println(iteration + ") ERROR " + " resolved PC : " + results.size());
    }
  }

  private TransformerService getTransformerService() {
//    return transformerService;
    Gson gson = new Gson();
    EmbeddedDataFileObjectProvider embeddedDataFileObjectProvider = new EmbeddedDataFileObjectProvider(
            new ResourceObjectService(null, null, gson));
    SpartanUtil spartanUtil = new SpartanUtil();
    TransformerFactory factory = new TransformerFactory(
            new JsTransformerExecutor(ZetaHardenedScriptEngineFactory::getInstance,
                    embeddedDataFileObjectProvider, spartanUtil, gson));
    return new TransformerService(embeddedDataFileObjectProvider, factory);
  }

  private static TransformerFunction getTransformerFunction(List<Argument> arguments, String functionName) {
    return TransformerFunction.builder().name(functionName).arguments(arguments).build();
  }

  public void simulate(int iteration, long tenantID) {
    Map<String, JsonElement> transformerArguments = getTransformerArguments(getIsoMessageAttributes());
    List<CompletableFuture<JsonElement>> ruleFutures = paymentCodes.stream()
            .map(paymentCode -> {
              System.out.println(iteration + ") PC:  " + paymentCode.getCode() + " " + paymentCode.getName());
              return buildTransformer(getFirstRule(paymentCode).getFunction(), "applyRule");
            })
            .map(transformer -> {
//          System.out.println("applying filtering : " + ifi + " : ");
              return transformerService.apply(tenantID, transformer, transformerArguments);
            })
            .map(CompletionStage::toCompletableFuture).collect(Collectors.toList());
    CompletableFuture.allOf(ruleFutures.toArray(new CompletableFuture[0]))
            .thenApply(aVoid -> ruleFutures.stream().map(CompletableFuture::join).collect(Collectors.toList()))
            .thenApply(listE -> listE.stream().map(JsonElement::getAsString).collect(Collectors.toList()))
            .thenAccept(responses1 -> {
              long resolvedPaymentCodeCount = responses1.stream().filter("PASSED"::equalsIgnoreCase).count();
              if (resolvedPaymentCodeCount != 1) {
                System.out.println("Race condition in iteration: " + iteration + ". Found matches: "
                        + resolvedPaymentCodeCount);
              }
            }).join();
  }

    private Map<String, Transformer> getTransformerMap() {
    Map<String, Transformer> transformerMap = new HashMap<>();
      paymentCodes.forEach(paymentCode -> transformerMap.put(paymentCode.getName(), buildTransformer(getFirstRule(paymentCode).getFunction(), "applyRule")));
      return transformerMap;
    }

  /**
   * Each payment code has only one rule. Hence, this simplification
   *
   * @param paymentCode Payment Code to extract rule from
   * @return First rule specified in the payment code
   */
  private Rule getFirstRule(PaymentCode paymentCode) {
    return paymentCode.getConnectorSpecification().getFilters().get(0);
  }

  private Transformer buildTransformer(String functionDefinition, String functionName) {
    return Transformer.builder().script(getTransformerScript(functionDefinition))
        .extensionPointID("defaultExtensionPointID").state(ResourceState.ENABLED)
        .chainSequenceNum(1)
        .function(getTransformerFunction(getTransformerArgumentsPlaceHolder(), functionName))
        .build();
  }

  private TransformerScript getTransformerScript(String function) {
    return TransformerScript.builder()
            .type(TransformerScriptType.INLINE)
            .format(ScriptFormat.JS)
            .content(function)
            .build();
  }

  private List<TransformerFunction.Argument> getTransformerArgumentsPlaceHolder() {
    List<TransformerFunction.Argument> argumentsList = new ArrayList<>();
    argumentsList.add(
        new TransformerFunction.Argument("inputPayload", new JsonPrimitive("$inputPayload")));
    return argumentsList;
  }

  private Map<String, JsonElement> getTransformerArguments(ISOMessageAttributes inputPayload) {
    Map<String, JsonElement> transformerArguments = new HashMap<>();
    transformerArguments.put("inputPayload", gson.toJsonTree(inputPayload));
    return transformerArguments;
  }

  private ISOMessageAttributes getIsoMessageAttributes() {
    Map<String, String> map = new HashMap<>();
    map.put("49", "986");
    map.put("22.2", "0");
    map.put("22.1", "10");
    map.put("61.4", "5");
    map.put("61.3", "2");
    map.put("61.6", "0");
    map.put("61.5", "1");
    map.put("51", "986");
    map.put("61.8", "0");
    map.put("61.7", "0");
    map.put("32", "009685");
    map.put("10", "64251900");
    map.put("61.9", "0");
    map.put("11", "000004");
    map.put("12", "150514");
    map.put("13", "1019");
    map.put("14", "4112");
    map.put("37", "123456789012");
    map.put("15", "1019");
    map.put("16", "1019");
    map.put("61.2", "0");
    map.put("18", "5999");
    map.put("61.1", "1");
    map.put("48.42.1.3", "2");
    map.put("0", "0100");
    map.put("2", "5568291192870502");
    map.put("4", "000000001000");
    map.put("48.92", "622");
    map.put("5", "000000001000");
    map.put("48.42.1.2", "1");
    map.put("3.1", "00");
    map.put("6", "000000001500");
    map.put("48.42.1.1", "2");
    map.put("3.2", "00");
    map.put("7", "1019150514");
    map.put("3.3", "00");
    map.put("9", "61000000");
    map.put("61.14", "6338500000");
    map.put("61.13", "840");
    map.put("61.12", "00");
    map.put("61.11", "0");
    map.put("61.10", "6");
    map.put("63", "MCS01106B");
    map.put("41", "Reg00004");
    map.put("42", "687555537877464");
    map.put("43", "Midwest Emporium       Columbia      MO");
    return new ISOMessageAttributes(map);
  }

  private static class Constants {

    enum TxnType {
      ACCOUNT_STATUS_INQUIRY("asi", "ACCOUNT_STATUS_INQUIRY", "PMCBLZZ01001"),
      ECOM_PREAUTH("preauth", "ECOM_PREAUTH", "PMCBLZZ01002"),
      ECOM_PREAUTH_RECURRING("preauth.recurring", "ECOM_PREAUTH_RECURRING", "PMCBLZZ01003"),
      RECURRING_PAYMENT("recurring", "RECURRING_PAYMENT", "PMCBLZZ01004"),
      POS_PURCHASE("pos", "POS_PURCHASE", "PMCBLZZ01005"),
      CONTACTLESS("contactless", "CONTACTLESS", "PMCBLZZ01006"),
      ECOM_PURCHASE("ecom", "ECOM_PURCHASE", "PMCBLZZ01007"),
      ATM_WITHDRAWAL("atm", "ATM_WITHDRAWAL", "PMCBLZZ01008");

      private final String type;
      private final String name;
      private final String paymentCode;

      TxnType(String type, String name, String paymentCode) {
        this.type = type;
        this.name = name;
        this.paymentCode = paymentCode;
      }

      public String getType() {
        return type;
      }

      public String getPaymentCode() {
        return paymentCode;
      }

      public String getName() {
        return name;
      }
    }
  }


}
