{
  "id": "ba6cc63f-232b-4fa9-bf6d-e6da7c72e81e",
  "ifi": 140827,
  "code": "PMCBLZZ01001",
  "name": "ACCOUNT_STATUS_INQUIRY",
  "description": "ACCOUNT_STATUS_INQUIRY",
  "status": "ACTIVE",
  "connectorSpecification": {
    "filters": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyRule(inputPayload) { var conditions = { \"3.1\": {\"allowedValues\":[\"00\"],\"presence\":\"M\"},\"22.1\": {\"allowedValues\":[\"81\",\"10\"],\"presence\":\"M\"},\"61.4\": {\"allowedValues\":[\"0\",\"1\",\"2\",\"3\",\"5\"],\"presence\":\"M\"},\"61.7\": {\"allowedValues\":[\"8\"],\"presence\":\"M\"} }; var passThroughFlag = \"PASSED\"; if (inputPayload.attributes) { var attributes = inputPayload.attributes; var key; for (key in conditions) { var val=attributes[key]; if (val==null) { if (conditions[key].presence==\"M\") passThroughFlag=\"FAILED\"; } else { var validValues=conditions[key].allowedValues; if (validValues) { if (validValues.indexOf(val) == -1) passThroughFlag=\"FAILED\"; } } } } return passThroughFlag; }"
      }
    ],
    "requestTransformers": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyTransformer(inputPayload) {var paymentMessage={};if (inputPayload.attributes) { var attributes = inputPayload.attributes; var money={}; money['amount'] = attributes['6']; money['currency'] = attributes['51'];paymentMessage['systemTraceAuditNumber'] = attributes['11'];paymentMessage['cardAcceptorBizCode'] = attributes['26'];paymentMessage['amountTransaction'] = money;} return paymentMessage; }"
      }
    ],
    "validators": [
      "CURRENCY_VALIDATOR"
    ]
  },
  "additionalAttributes": {
    "txnType": "ACCOUNT_STATUS_INQUIRY"
  }
}