{
  "id": "a5f7aff3-891a-43ce-a562-0290318cc5f9",
  "ifi": 140827,
  "code": "PMCBLZZ01007",
  "name": "ECOM_PURCHASE",
  "description": "ECOM_PURCHASE",
  "status": "ACTIVE",
  "connectorSpecification": {
    "filters": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyRule(inputPayload) { var conditions = { \"22.1\": {\"allowedValues\":[\"81\",\"10\"],\"presence\":\"M\"},\"48.42.1.3\": {\"allowedValues\":[\"2\"],\"presence\":\"M\"},\"61.4\": {\"allowedValues\":[\"0\",\"1\",\"2\",\"3\",\"5\"],\"presence\":\"M\"},\"61.7\": {\"allowedValues\":[\"0\",\"2\",\"5\"],\"presence\":\"M\"} }; var passThroughFlag = \"PASSED\"; if (inputPayload.attributes) { var attributes = inputPayload.attributes; var key; for (key in conditions) { var val=attributes[key]; if (val==null) { if (conditions[key].presence==\"M\") passThroughFlag=\"FAILED\"; } else { var validValues=conditions[key].allowedValues; if (validValues) { if (validValues.indexOf(val) == -1) passThroughFlag=\"FAILED\"; } } } } return passThroughFlag; }"
      }
    ],
    "requestTransformers": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyTransformer(inputPayload) {var paymentMessage={};if (inputPayload.attributes) { var attributes = inputPayload.attributes; var money={}; money['amount'] = attributes['6']; money['currency'] = attributes['51'];paymentMessage['systemTraceAuditNumber'] = attributes['11'];paymentMessage['cardAcceptorBizCode'] = attributes['26'];paymentMessage['amountTransaction'] = money;} return paymentMessage; }"
      }
    ],
    "validators": [
      "CURRENCY_VALIDATOR", "CVV2_PRESENCE_VALIDATOR"
    ]
  },
  "additionalAttributes": {
    "txnType": "ECOM_PURCHASE"
  }
}