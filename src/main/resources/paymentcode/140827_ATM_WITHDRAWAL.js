{
  "id": "7527b75c-b3a1-4cda-87f5-6a4b2ffbf515",
  "ifi": 140827,
  "code": "PMCBLZZ01008",
  "name": "ATM_WITHDRAWAL",
  "description": "ATM_WITHDRAWAL",
  "status": "ACTIVE",
  "connectorSpecification": {
    "filters": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyRule(inputPayload) { var conditions = { \"3.1\": {\"allowedValues\":[\"01\",\"17\"],\"presence\":\"M\"},\"22.1\": {\"allowedValues\":[\"05\",\"80\",\"90\"],\"presence\":\"M\"},\"61.4\": {\"allowedValues\":[\"0\"],\"presence\":\"M\"},\"61.5\": {\"allowedValues\":[\"0\"],\"presence\":\"M\"},\"61.7\": {\"allowedValues\":[\"0\"],\"presence\":\"M\"} }; var passThroughFlag = \"PASSED\"; if (inputPayload.attributes) { var attributes = inputPayload.attributes; var key; for (key in conditions) { var val=attributes[key]; if (val==null) { if (conditions[key].presence==\"M\") passThroughFlag=\"FAILED\"; } else { var validValues=conditions[key].allowedValues; if (validValues) { if (validValues.indexOf(val) == -1) passThroughFlag=\"FAILED\"; } } } } return passThroughFlag; }"
      }
    ],
    "requestTransformers": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyTransformer(inputPayload) {var paymentMessage={};if (inputPayload.attributes) { var attributes = inputPayload.attributes; var money={}; money['amount'] = attributes['6']; money['currency'] = attributes['51'];paymentMessage['systemTraceAuditNumber'] = attributes['11'];paymentMessage['cardAcceptorBizCode'] = attributes['26'];paymentMessage['amountTransaction'] = money;} return paymentMessage; }"
      }
    ],
    "validators": [
      "CURRENCY_VALIDATOR"
    ]
  },
  "additionalAttributes": {
    "txnType": "ATM_WITHDRAWAL"
  }
}