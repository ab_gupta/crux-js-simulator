{
  "id": "68bf6d7b-d8e0-4c58-9a7f-40945a446330",
  "ifi": 140827,
  "code": "PMCBLZZ01003",
  "name": "ECOM_PREAUTH_RECURRING",
  "description": "ECOM_PREAUTH_RECURRING",
  "status": "ACTIVE",
  "connectorSpecification": {
    "filters": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyRule(inputPayload) { var conditions = { \"22.1\": {\"allowedValues\":[\"81\",\"10\"],\"presence\":\"M\"},\"61.4\": {\"allowedValues\":[\"4\"],\"presence\":\"M\"},\"61.7\": {\"allowedValues\":[\"4\"],\"presence\":\"M\"},\"61.10\": {\"allowedValues\":[\"0\",\"6\"],\"presence\":\"M\"} }; var passThroughFlag = \"PASSED\"; if (inputPayload.attributes) { var attributes = inputPayload.attributes; var key; for (key in conditions) { var val=attributes[key]; if (val==null) { if (conditions[key].presence==\"M\") passThroughFlag=\"FAILED\"; } else { var validValues=conditions[key].allowedValues; if (validValues) { if (validValues.indexOf(val) == -1) passThroughFlag=\"FAILED\"; } } } } return passThroughFlag; }"
      }
    ],
    "requestTransformers": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyTransformer(inputPayload) {var paymentMessage={};if (inputPayload.attributes) { var attributes = inputPayload.attributes; var money={}; money['amount'] = attributes['6']; money['currency'] = attributes['51'];paymentMessage['systemTraceAuditNumber'] = attributes['11'];paymentMessage['cardAcceptorBizCode'] = attributes['26'];paymentMessage['amountTransaction'] = money;} return paymentMessage; }"
      }
    ],
    "validators": [
      "CURRENCY_VALIDATOR"
    ]
  },
  "additionalAttributes": {
    "txnType": "ECOM_PREAUTH_RECURRING"
  }
}