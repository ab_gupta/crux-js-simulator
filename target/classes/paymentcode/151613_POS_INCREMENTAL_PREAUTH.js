{
  "id": "a5f7aff3-891a-43ce-a562-0290318cc8w2",
  "ifi": 151613,
  "code": "PMCINZZ0062",
  "name": "POS_INCREMENTAL_PREAUTH",
  "description": "POS_INCREMENTAL_PREAUTH",
  "status": "ACTIVE",
  "connectorSpecification": {
    "filters": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyRule(inputPayload) {var conditions = {\"3.1\": {\"allowedValues\": [\"00\"], \"presence\": \"M\"}, \"22.1\": {\"allowedValues\": [\"00\", \"05\"], \"presence\": \"M\"}, \"48.61.5\": {\"allowedValues\": [\"0\"], \"presence\": \"M\"}, \"48.63\": {\"presence\": \"M\"}, \"61.1\": {\"allowedValues\": [\"0\"], \"presence\": \"M\"}, \"61.3\": {\"allowedValues\": [\"0\", \"4\"], \"presence\": \"M\"}, \"61.4\": {\"allowedValues\": [\"0\"], \"presence\": \"M\"}, \"61.5\": {\"allowedValues\": [\"0\"], \"presence\": \"M\"}, \"61.7\": {\"allowedValues\": [\"4\"], \"presence\": \"M\"}, \"61.10\": {\"allowedValues\": [\"0\", \"2\", \"3\", \"4\", \"9\"], \"presence\": \"M\"}, \"61.11\": {\"allowedValues\": [\"0\", \"3\", \"5\", \"6\", \"8\", \"9\"], \"presence\": \"M\"} }; var passThroughFlag = \"PASSED\"; if (inputPayload.attributes) {var attributes = inputPayload.attributes; var key; for (key in conditions) {var val = attributes[key]; if (val == null) {if (conditions[key].presence == \"M\") passThroughFlag = \"FAILED\"; } else { if(conditions[key].presence == \"A\") { passThroughFlag = \"FAILED\"; } else { var validValues = conditions[key].allowedValues; if (validValues) {if (validValues.indexOf(val) == -1) passThroughFlag = \"FAILED\"; } } } } } return passThroughFlag; }"
      }
    ],
    "requestTransformers": [],
    "validators": [
      "CURRENCY_VALIDATOR"
    ]
  },
  "additionalAttributes": {
    "txnType": "POS_INCREMENTAL_PREAUTH"
  }
}