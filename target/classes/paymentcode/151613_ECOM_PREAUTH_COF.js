{
  "id": "a5f7aff3-891a-43ce-a562-0290318cc5f9",
  "ifi": 151613,
  "code": "PMCINZZ0060",
  "name": "ECOM_PREAUTH_COF",
  "description": "ECOM_PREAUTH_COF",
  "status": "ACTIVE",
  "connectorSpecification": {
    "filters": [
      {
        "type": "JAVASCRIPT",
        "function": "function applyRule(inputPayload) {var conditions = {\"3.1\": {\"allowedValues\": [\"00\"], \"presence\": \"M\"}, \"22.1\": {\"allowedValues\": [\"10\"], \"presence\": \"M\"}, \"48.42.1.3\": {\"allowedValues\": [\"2\"], \"presence\": \"M\"}, \"48.61.5\": {\"allowedValues\": [\"0\"], \"presence\": \"O\"}, \"61.1\": {\"allowedValues\": [\"1\", \"2\"], \"presence\": \"M\"}, \"61.3\": {\"allowedValues\": [\"2\", \"4\"], \"presence\": \"M\"}, \"61.4\": {\"allowedValues\": [\"0\", \"5\"], \"presence\": \"M\"}, \"61.5\": {\"allowedValues\": [\"1\"], \"presence\": \"M\"}, \"61.6\": {\"allowedValues\": [\"0\"], \"presence\": \"M\"}, \"61.7\": {\"allowedValues\": [\"4\"], \"presence\": \"M\"}, \"61.10\": {\"allowedValues\": [\"0\", \"6\"], \"presence\": \"M\"}, \"61.11\": {\"allowedValues\": [\"0\", \"1\", \"6\"], \"presence\": \"M\"} }; var passThroughFlag = \"PASSED\"; if (inputPayload.attributes) {var attributes = inputPayload.attributes; var key; for (key in conditions) {var val = attributes[key]; if (val == null) {if (conditions[key].presence == \"M\") passThroughFlag = \"FAILED\"; } else {var validValues = conditions[key].allowedValues; if (validValues) {if (validValues.indexOf(val) == -1) passThroughFlag = \"FAILED\"; } } } } return passThroughFlag; }"
      }
    ],
    "requestTransformers": [],
    "validators": [
      "CURRENCY_VALIDATOR", "CVV2_PRESENCE_VALIDATOR"
    ]
  },
  "additionalAttributes": {
    "txnType": "ECOM_PREAUTH"
  }
}